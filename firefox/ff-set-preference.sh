#!/bin/bash

FIREFOXPATH="$HOME/.mozilla/firefox"
PROFILESINIPATH="$FIREFOXPATH/profiles.ini"

PROFILENAME=$(grep -E '^\[Profile|^Path|^Default' $PROFILESINIPATH | grep -1 '\[Profile0\]' | grep '^Path' | sed 's/^Path=//')
PROFILEPATH="$FIREFOXPATH/$PROFILENAME"

touch $PROFILEPATH/user.js
sed -i 's/user_pref("'$1'",.*);/user_pref("'$1'", '$2');/' $PROFILEPATH/user.js
grep -q $1 $PROFILEPATH/user.js || echo "user_pref(\"$1\", $2);" >> $PROFILEPATH/user.js

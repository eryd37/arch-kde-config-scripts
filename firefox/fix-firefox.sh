#!/bin/bash

# Get path of firefox profile directory
FIREFOXPATH="$HOME/.mozilla/firefox"
PROFILESINIPATH="$FIREFOXPATH/profiles.ini"

PROFILENAME=$(grep -E '^\[Profile|^Path|^Default' $PROFILESINIPATH | grep -1 '\[Profile0\]' | grep '^Path' | sed 's/^Path=//')
PROFILEPATH="$FIREFOXPATH/$PROFILENAME"
echo $PROFILEPATH

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Remove tab bar
mkdir $PROFILEPATH/chrome
touch $PROFILEPATH/chrome/userChrome.css
echo "#TabsToolbar { visibility: collapse !important; }" > $PROFILEPATH/chrome/userChrome.css
bash $SCRIPT_DIR/ff-set-preference.sh toolkit.legacyUserProfileCustomizations.stylesheets 'true'

# Use KDE dialogs for files
#sudo pacman -Syu xdg-desktop-portal xdg-desktop-portal-kde
bash $SCRIPT_DIR/ff-set-preference.sh widget.use-xdg-desktop-portal 'true'
bash $SCRIPT_DIR/ff-set-preference.sh widget.use-xdg-desktop-portal.file-picker '1'
bash $SCRIPT_DIR/ff-set-preference.sh widget.use-xdg-desktop-portal.mime-handler '1'

bash $SCRIPT_DIR/ff-set-preference.sh extensions.pocket.enabled 'false'

# Make middle mouse button autoscroll, not paste
bash $SCRIPT_DIR/ff-set-preference.sh general.autoScroll 'true'
bash $SCRIPT_DIR/ff-set-preference.sh middlemouse.paste 'false'

# https://wiki.archlinux.org/title/Firefox#Hardware_video_acceleration
bash $SCRIPT_DIR/ff-set-preference.sh media.ffmpeg.vaapi.enabled 'true'

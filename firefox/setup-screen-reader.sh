#!/bin/bash

sudo pacman -Syu espeak-ng speech-dispatcher orca
sudo sed -i 's/#AddModule "espeak-ng"\s*"sd_espeak-ng" "espeak-ng.conf"/AddModule "espeak-ng"                 "sd_espeak-ng" "espeak-ng.conf"/' /etc/speech-dispatcher/speechd.conf
spd-conf -s
echo "Enable screen reader in settings and restart in order to use screen reader"

#!/bin/bash
echo "Setting up package management"
bash ./package-manager/setup-package-management.sh

echo "Installing fish"
bash ./shell/install-fish.sh
bash ./shell/setup-fish-config.sh

echo "Fixing firefox settings"
bash ./firefox/fix-firefox.sh

echo "Fixing git with ssh on VSCode"
bash ./vscode/fix-vscode-ssh.sh

echo "Setting up antivirus (ClamAV)"
bash ./setup-clamav.sh

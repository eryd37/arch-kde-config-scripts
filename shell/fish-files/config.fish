if status is-interactive 
	# Commands to run in interactive sessions can go here

	set -g fish_greeting

	bind \cright forward-word
	bind \cleft backward-word
	bind \cH backward-kill-word
	bind \e\[3\;5~ kill-word

	export EDITOR='nano'
	export PATH='/bin:/usr/bin:/sbin:/usr/sbin:/home/simpliston/.local/bin:/usr/local/bin:/var/lib/snapd/snap/bin:/home/simpliston/.nvm/versions/node/v20.11.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/simpliston/.dotnet/tools'

	# pnpm
    export PNPM_HOME="/home/simpliston/.local/share/pnpm"
    if not contains ":$PNPM_HOME:" ":$PATH:"
        export PATH="$PNPM_HOME:$PATH"
    end
    # pnpm end

    export BUN_HOME="/home/simpliston/.bun/bin"
    if not contains ":$BUN_HOME:" ":$PATH:"
        export PATH="$BUN_HOME:$PATH"
    end
	
	alias yeet 'sudo pacman -Rns'
	alias ud 'sudo pacman -Syu'
	alias ug 'sudo pacman -Syyu'
	alias orphans 'pacman -Qtdq'
	alias removeorphans 'sudo pacman -Rnu (orphans)'
	alias what 'pacman -Qi'
	alias why 'pactree -r'
	
	alias tree='tree -LC 2'
	alias ls='ls --color=auto'
	alias untar='tar -xvf'
	alias please='sudo (fc -ln -1)'
	alias pls='please'
	alias myip='curl ipinfo.io/ip'
	alias se='sudoedit'
	alias wifipass='nmcli device wifi show-password'
	alias dangermode='printf %b "\e]11;#400000\a"'
	alias ready-ssh='eval (ssh-agent -c) && ssh-add ~/.ssh/id_ed25519'
	alias new-branch='git switch --create'
	alias ga='git add -i'
	alias gc='git fetch --prune && git branch -vv | grep ": gone]"| grep -v "\*" | awk "{ print \$1; }" | xargs -r git branch -d'
	alias gh='git switch (git symbolic-ref refs/remotes/origin/HEAD | sed "s@^refs/remotes/origin/@@")'
	alias gl='git pull'
	alias glg='git log --date=format:"%a %b %e, %Y" --pretty=format:"%C(yellow)%h%C(reset) %s %C(cyan)%cd%C(reset) %C(blue)%an%C(reset) %C(green)%d%C(reset)" --graph'
	alias gm='git commit -m'
	alias gp='git push'
	alias gs='git status'
	alias gu='git restore .'
	alias gup='git pull --rebase'
	alias openrepo='xdg-open (git remote get-url origin | sed "s/git@//" | sed "s/\.git//" | tr ":" "/")'
	alias grepi='grep -rni --exclude-dir="obj" --exclude-dir="bin" --exclude-dir="publish" --exclude-dir="node_modules" --exclude-dir=".angular" --exclude-dir=".vscode" --exclude-dir=".vs" --exclude-dir="dist" --exclude="package-lock.json" --exclude="yarn.lock" --exclude="pnpm-lock.yaml"'
	alias rgi='rg -i -g "!pnpm-lock.yaml"'

	function to-kebab -a text
		echo "$text" | tr "A-Z" "a-z" | tr -d ";:,." | sed "s/\// /g" | tr " " "-" | tr -d \\n | wl-copy
	end

end

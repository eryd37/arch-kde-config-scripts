#!/bin/bash

sed -i "/alias $1=.*/d" $HOME/.zshrc
echo "alias $1='$2'" >> $HOME/.zshrc

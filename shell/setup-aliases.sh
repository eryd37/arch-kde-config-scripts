#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

distro=$(lsb_release -si)
case $distro in
  Arch | EndeavourOS)
    echo "Detected Arch based distro, setting up aliases for pacman"
    bash $SCRIPT_DIR/set-alias.sh yeet 'sudo pacman -Rns'
    bash $SCRIPT_DIR/set-alias.sh ud 'sudo pacman -Syu'
    bash $SCRIPT_DIR/set-alias.sh ug 'sudo pacman -Syyu'
    bash $SCRIPT_DIR/set-alias.sh orphans 'pacman -Qtdq'
    bash $SCRIPT_DIR/set-alias.sh removeorphans 'sudo pacman -Rnu $(orphans)'
    bash $SCRIPT_DIR/set-alias.sh what 'pacman -Qi'
    bash $SCRIPT_DIR/set-alias.sh why 'pactree -r'
    ;;
  *)
    echo "Unknown distro, skipping package manager aliases"
    ;;
esac

bash $SCRIPT_DIR/set-alias.sh tree 'tree -LC 2'
bash $SCRIPT_DIR/set-alias.sh ls 'ls --color=auto'
bash $SCRIPT_DIR/set-alias.sh untar 'tar -xvf'
bash $SCRIPT_DIR/set-alias.sh please 'sudo $(fc -ln -1)'
bash $SCRIPT_DIR/set-alias.sh pls 'please'
bash $SCRIPT_DIR/set-alias.sh myip 'curl ipinfo.io/ip'
bash $SCRIPT_DIR/set-alias.sh se 'sudoedit'
bash $SCRIPT_DIR/set-alias.sh wifipass 'nmcli device wifi show-password'
bash $SCRIPT_DIR/set-alias.sh dangermode 'printf %b "\e]11;#400000\a"'
bash $SCRIPT_DIR/set-alias.sh ready-ssh 'eval "$(ssh-agent -s)" && ssh-add ~/.ssh/id_ed25519'
bash $SCRIPT_DIR/set-alias.sh new-branch 'git switch --create'
bash $SCRIPT_DIR/set-alias.sh gs 'git status'
bash $SCRIPT_DIR/set-alias.sh gh 'git switch $(git symbolic-ref refs/remotes/origin/HEAD | sed "s@^refs/remotes/origin/@@")'
bash $SCRIPT_DIR/set-alias.sh gup 'git pull --rebase'
bash $SCRIPT_DIR/set-alias.sh gc 'git fetch --prune && git branch -vv | grep ": gone]"|  grep -v "\*" | awk "{ print \$1; }" | xargs -r git branch -d'
bash $SCRIPT_DIR/set-alias.sh gm 'git commit -m'
bash $SCRIPT_DIR/set-alias.sh ga 'git add -i'
bash $SCRIPT_DIR/set-alias.sh gu 'git restore .'
bash $SCRIPT_DIR/set-alias.sh gp 'git push'
bash $SCRIPT_DIR/set-alias.sh glg 'git log --date=format:"%a %b %e, %Y" --pretty=format:"%C(yellow)%h%C(reset) %s %C(cyan)%cd%C(reset) %C(blue)%an%C(reset) %C(green)%d%C(reset)" --graph'
bash $SCRIPT_DIR/set-alias.sh openrepo 'xdg-open $(git remote get-url origin | sed "s/git@//" | sed "s/\.git//" | tr ":" "/")'
bash $SCRIPT_DIR/set-alias.sh grepi 'grep -rni --exclude-dir="node_modules"'
bash $SCRIPT_DIR/set-alias.sh greps 'grep -rn --exclude-dir="node_modules"'

#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cp $SCRIPT_DIR/fish-files/config.fish ~/.config/fish/config.fish
cp $SCRIPT_DIR/fish-files/fish_prompt.fish ~/.config/fish/functions/fish_prompt.fish
cp $SCRIPT_DIR/fish-files/fish_right_prompt.fish ~/.config/fish/functions/fish_right_prompt.fish

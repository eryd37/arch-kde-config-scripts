#!/bin/bash

date=$(date +%d.%m.%y)
wayland_or_x11=$(echo $XDG_SESSION_TYPE)
if [[ "$wayland_or_x11" == "wayland" ]]; then
  if hash wl-copy 2>/dev/null; then
    wl-copy "$date"
  else
    echo "wl-copy is not installed: https://github.com/bugaevc/wl-clipboard"
  fi
elif [[ "$wayland_or_x11" == "x11" ]]; then
  echo "$date" | xclip -sel clip
fi
#!/bin/bash

ISSUE_NUMBER=$(git status | grep -E -o -m 1 '[A-Z]+-[0-9]+')
git commit -m "$ISSUE_NUMBER $1"
